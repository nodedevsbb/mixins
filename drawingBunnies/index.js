if (!this.username) {
    this.setUsername();
} // bad

this.username || this.setUsername() // good (a username exists, or set a username.)

/////////////////////////////////

if (users.indexOf(this.username) === -1) {
    users.push(this.username)
} // bad

~users.indexOf(this.username) || users.push(this.username) // good (the username is in the array, or add it.)

// The bitwise NOT (~) operator inverts the bits of its operand, turning a -1 into a 0 (or falsy).


/////////////////////////////////

function getOffset (x, y, w, h, placement) {
    var offset
    switch (placement) {
        case 'bottom':
            offset = {
                top: y + h,
                left: x + w/2
            };
            break;
        case 'top':
            offset = {
                top: y,
                left: x + w/2
            };
            break;
        case 'left':
            offset = {
                top: y + h/2,
                left: x
            };
            break;
        case 'right':
            offset = {
                top: y + h/2,
                left: x + w
            };
            break
    }
    return offset
}

function getOffsetBeautiful (x, y, w, h, placement) {
    return placement == 'bottom' ? { top: y + h, left: x + w/2 } :
        placement == 'top' ? { top: y, left: x + w/2 } :
            placement == 'left' ? { top: y + h/2, left: x } :
            { top: y + h/2, left: x + w }
}

function getOffsetMoreBeautiful (x, y, w, h, placement) {
    return {
        top : placement == 'bottom' ? y + h :
            placement == 'top' ? y : y + h/2,
        left : placement == 'right' ? x + w :
            placement == 'left' ? x : x + w/2
    }
}

//////////////////////////////////
/*
 Consider immediately invoked function expressions (IIFEs). By convention, an IIFE
 takes one of the two following forms
*/
(function (){})();
(function (){}());

!function (){}();
~function (){}();
+function (){}();
-function (){}();
new function (){};
1,function (){}();
1 && function (){}();
var i=function (){}();

/*
 Each manifestation has its own unique qualities and advantages—some with fewer
 bytes, some safer for concatenation, each valid and each executable.
*/

///////////////////////////////////////

