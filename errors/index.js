function doSomething() {
    try {
        functionThatMightThrowError();
        return "success";
    } catch(error) {
        return "failure";
    } finally {
        return "finally";
    }
}
function doSomethingElse() {
    try {
        functionThatMightThrowError();
        return "success";
    } catch(error) {
        return "failure";
    }
    return "finally";
}

var result1 = doSomething();
var result2 = doSomethingElse();

/* The value of result1 is always "finally", regardless of  whether an error occurs.
 The value of result2 will be "success" if there is no error and "failure" if there is an error.
 */


/////////////// Custom errors

function MyError(message) {
    this.message = message;
}
MyError.prototype = Object.create(Error.prototype);

function MissingArgumentError(message) {
    this.message = message;
}
MissingArgumentError.prototype = Object.create(MyError.prototype);

function NotFunnyError(message) {
    this.message = message;
}

NotFunnyError.prototype = Object.create(MyError.prototype);

if (error instanceof MyError) {
// handle MissingArgumentError and NotFunnyError
} else {
    // handle native error types
}
