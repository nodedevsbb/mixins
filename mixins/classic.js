var circleFns = {
    area: function() {
        return Math.PI * this.radius * this.radius;
    },
    grow: function() {
        this.radius++;
    },
    shrink: function() {
        this.radius--;
    }
};

var clickableFns = {
    hover: function() {
        console.log('hovering');
    },
    press: function() {
        console.log('button pressed');
    },
    release: function() {
        console.log('button released');
    },
    fire: function() {
        this.action.fire();
    }
};

function extend(destination, source) {
    for (var key in source) {
        if (source.hasOwnProperty(key)) {
            destination[key] = source[key];
        }
    }
    return destination;
}

var RoundButton = function(radius, label) {
    this.radius = radius;
    this.label = label;
};

extend(RoundButton.prototype, circleFns);
extend(RoundButton.prototype, clickableFns);

var roundButton = new RoundButton(3, 'send');

roundButton.grow();
roundButton.fire();