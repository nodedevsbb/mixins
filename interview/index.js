// 1. write explode function
if ('alex'.explode() === 'a l e x') interview.nextQuestion();
else interview.terminate();

// the worst answer
String.prototype.explode = function () {
    var i;
    var result = '';
    for (i = 0; i < this.length; i++) {
        result = result + this[i];
        if (i < result.length - 1) {
            result = result + ' '
        }
    }
    return result
};

// beautiful solution
String.prototype.explode = function (f,a,t) {
    for (f = a = '', t = this.length; a++ < t;) {
        f += this[a-1];
        a < t && (f += ' ');
    }
    return f;
};

/* The for loop saves some characters by
 setting both f and a to new string, and the a is then coerced in the next expression to 1
 by the ++ increment operator_, just in time to be used in the equality comparison. */

// The next iteration of the solution is by far the simplest, leaning heavily on the language’s tool belt.

String.prototype.explode = function () {
    return this.split('').join(' ')
};

// the absolute simplest:
String.prototype.explode = function (/*smart a$$*/) {
    return 'a l e x'
};







//////////////////////////////////////////

["10", "20", "lol"].map(Number);
// → [10, 20, NaN]

["10", "20", "lol"].map(Number).filter(Boolean);
// → [10, 20]


/*
[].map also takes a second argument, and that’s a calling context, or thisArg. This is
 the object that will serve as this when callback is running on the values:
 */
["lol", "wow", "ok"].map(function(string) {
    return this[string];
}, {
    lol: "yeah!",
    wow: "alright!",
    ok: "cool!"
});
// → ["yeah!", "alright!", "cool!"]


/////////////////////////////////////////

JSON.stringify({ foo: "bar" });
// -> "{"foo":"bar"}"

Object.prototype.toJSON = function () {
    return "reality ain't always the truth"
};
JSON.stringify({ foo: "bar" });
// -> ""reality ain't always the truth""

