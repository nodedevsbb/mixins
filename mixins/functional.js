var withCircle = function() {
    this.area = function() {
        return Math.PI * this.radius * this.radius;
    };
    this.grow = function() {
        this.radius++;
    };
    this.shrink = function() {
        this.radius--;
    };
};
var withClickable = function() {
    this.hover = function() {
        console.log('hovering');
    };
    this.press = function() {
        console.log('button pressed');
    };
    this.release = function() {
        console.log('button released');
    };
    this.fire = function() {
        console.log(this.action());
    };
}

var RoundButton = function(radius, label, action) {
    this.radius = radius;
    this.label = label;
    this.action = action;
};

withCircle.call(RoundButton.prototype);
withClickable.call(RoundButton.prototype);

var button1 = new RoundButton(4, 'yes!', function() {return 'you said yes!'});
button1.fire(); //'you said yes!'


// Adding Options
var withOval = function(options) {
    this.area = function() {
        return Math.PI * this.longRadius * this.shortRadius;
    };
    this.ratio = function() {
        return this.longRadius/this.shortRadius;
    };
    this.grow = function() {
        this.shortRadius += (options.growBy/this.ratio());
        this.longRadius += options.growBy;
    };
    this.shrink = function() {
        this.shortRadius -= (options.shrinkBy/this.ratio());
        this.longRadius -= options.shrinkBy;
    };
}
var OvalButton = function(longRadius, shortRadius, label, action) {
    this.longRadius = longRadius;
    this.shortRadius = shortRadius;
    this.label = label;
    this.action = action;
};

withClickable.call(OvalButton.prototype);
withOval.call(OvalButton.prototype, {growBy: 2, shrinkBy: 2});

var button2 = new OvalButton(3, 2, 'send', function() {return 'message sent'});
console.log(button2.area()); //18.84955592153876
button2.grow();
console.log(button2.area()); //52.35987755982988
button2.fire(); //'message sent'


// Adding Caching

var withRectangle = (function() {
    function area() {
        return this.length * this.width;
    }
    function grow() {
        this.length++;
        this.width++;
    }
    function shrink() {
        this.length--;
        this.width--;
    }
    return function() {
        this.area = area;
        this.grow = grow;
        this.shrink = shrink;
        return this;
    };
})();

var RectangularButton = function(length, width, label, action) {
    this.length = length;
    this.width = width;
    this.label = label;
    this.action = action;
};

withClickable.call(RectangularButton.prototype);
withRectangle.call(RectangularButton.prototype);

var button3 = new RectangularButton(4, 2, 'delete', function() {return 'deleted'});
button3.area(); //8
button3.grow();
button3.area(); //15
button3.fire(); //'deleted'