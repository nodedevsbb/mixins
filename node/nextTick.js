//////////////////////////

/*Running this example will fail to trigger the success listener*/
var EventEmitter1 = require('events').EventEmitter;

function complexOperations1() {
    var events = new EventEmitter1();
    events.emit('success');
    return events;
}

complexOperations1().on('success', function() {
    console.log('success!');
});

///////////////////////////////////////

var EventEmitter = require('events').EventEmitter;

function complexOperations() {
    var events = new EventEmitter();

    process.nextTick(function() {
        events.emit('success'); /*The event will now
         be emitted when the
         listener is ready.*/
    });

    return events;
}

complexOperations().on('success', function() {
    console.log('success!');
});


//////////////////////////////////////////////

var fs = require('fs');
var content;

function readFileIfRequired(cb) {
    if (!content) {
        fs.readFile(__filename, 'utf8', function(err, data) {
            content = data;
            console.log('readFileIfRequired: readFile');
            cb(err, content);
        });
    }
    else {
        /*If the content has been read,
         pass the cached version to
         the callback, but first use
         process.nextTick to ensure
         the callback is executed later.*/

        process.nextTick(function() {
            console.log('readFileIfRequired: cached');
            cb(null, content);
        });
    }
}

readFileIfRequired(function(err, data) {
    console.log('1. Length:', data.length);

    readFileIfRequired(function(err, data2) {
        console.log('2. Length:', data2.length);
    });

    console.log('Reading file again...');
});

console.log('Reading file...');




/*
 The callbacks that have been passed with process.nextTick are usually run at the
 end of the current event loop. The number of callbacks that can be safely run is controlled
 by process.maxTickDepth, which is 1000 by default to allow I/O operations to
 continue to be handled.
*/