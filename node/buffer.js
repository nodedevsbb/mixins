var fs = require('fs');
fs.readFile('./names.txt', function (er, buf) {
    console.log(buf); // -> <Buffer 4a 61 6e 65 74 0a 57 6f 6f 6b 69 65 0a 41 6c 65 78 0a 4d 61 72 63 0a>
});


fs.readFile('./names.txt', function (er, buf) {
    /* toString by default will convert data into a UTF-8 encoded string. */
    console.log(buf.toString()); // -> This will yield the same output as our original file
});

/*
But since we know that this data is only comprised of ASCII characters, we could also
get a performance benefit by changing the encoding to ASCII rather than UTF-8. To
do this, we provide the type of encoding as the first argument for toString:
*/

fs.readFile('./names.txt', function (er, buf) {
    console.log(buf.toString('ascii'));
});

/////////////////////////////////////////

var buf = new Buffer('am9obm55OmMtYmFk', 'base64'); // allocate string into a Buffer, second argument is type of string, default is UTF-8
var encoded = buf.toString('base64'); // -> am9obm55OmMtYmFk

/////////////////////////////////////
// Working with data URIs

/*Data URIs4 are another example of when using the Buffer API can be helpful. Data URIs
 allow a resource to be embedded inline on a web page using the following scheme:
 data:[MIME-type][;charset=<encoding>[;base64],<data>
 */

var mime = 'image/png';
var encoding = 'base64';
var data = fs.readFileSync('./monkey.png').toString(encoding);
var uri = 'data:' + mime + ';' + encoding + ',' + data; // Construct data URI
console.log(uri);

// create image from data uri
uri = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACsAAAAo...';
data = uri.split(',')[1];
buf = Buffer(data, 'base64');
fs.writeFileSync('./secondmonkey.png', buf);

////////////////////////////////////////