var a = []; a[0] = 'var a = []; ';
a[1] = 'a[';
a[2] = '] = ';
a[3] = '\'';
a[4] = '\\';
a[5] = ';';
a[6] = '';
a[7] = 'for(var i = 0; i < a.length; i++) console.log((i == 0 ? a[0] : a[6])\
+ a[1] + i + a[2] + a[3] + ((i == 3 || i == 4) ? a[4] : a[6]) + a[i] + a[3] \
+ a[5] + (i == 7 ? a[7] : a[6]))'; for(var i = 0; i < a.length; i++) console.log((i == 0 ? a[0] : a[6]) + a[1] + i + a[2] + a[3] +
    ((i == 3 || i == 4) ? a[4] : a[6]) + a[i] + a[3] + a[5] +
    (i == 7 ? a[7] : a[6]))


//////////////////////////////

    (function f() { console.log('(' + f.toString() + ')()') })()

//////////////////////////////

/*Not all implementations of the quine are so accessible. This dense quine by Ben Alman
originally fit into a tweet (followed by “#quine,” of course):*/
    !function $(){console.log('!'+$+'()')}()
/*
Ben’s quine is conceptually identical to the quine 2, but compacted and
obscured as much as possible. The parentheses wrapping the function are traded for a
    leading ! operator—both ensure the function is interpreted as an expression. We then
rename the function to $ simply to throw a little spice into the mix—something that
feels like it should be an operator, but really isn’t. Inside the console.log statement, we
reuse the ! operator (this is a quine, after all) and concatenate it with the $ method
and trailing invocation parentheses. The toString method is nowhere to be found:
    concatenating a string with a function implicitly calls the function’s toString method.*/
